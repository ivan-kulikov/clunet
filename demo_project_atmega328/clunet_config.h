/* Name: clunet_config.h
 * Project: CLUNET network driver
 * Author: Alexey Avdyukhin
 * Creation Date: 2012-11-08
 * License: DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 * Atmega328 by gunmetal
 */
 
#ifndef __clunet_config_h_included__
#define __clunet_config_h_included__

/* Device address (0-254) */
#define CLUNET_DEVICE_ID 99

/* Device name */
#define CLUNET_DEVICE_NAME "CLUNET device 1"

/* Buffer sized (memory usage) */
#define CLUNET_SEND_BUFFER_SIZE 128
#define CLUNET_READ_BUFFER_SIZE 128

/* Pin to send data */
#define CLUNET_WRITE_PORT D
#define CLUNET_WRITE_PIN 3

/* Using transistor? */
//#define CLUNET_WRITE_TRANSISTOR

/* Pin to receive data, external interrupt required! */
#define CLUNET_READ_PORT D
#define CLUNET_READ_PIN 2

/* Timer prescaler */
#define CLUNET_TIMER_PRESCALER 64

/* Custom T (T >= 8 && T <= 24). 
 T is frame unit size in timer ticks. Lower - faster, highter - more stable
 If not defined T will be calculated as ~64us based on CLUNET_TIMER_PRESCALER value
*/
 //#define CLUNET_T 8

/* Timer initialization */
#define CLUNET_TIMER_INIT {unset_bit4(TCCR0A,COM0A1,WGM01, WGM00, COM0A0); /* Timer, normal mode */ \
	unset_bit(TCCR0B, CS02); set_bit2(TCCR0B, CS01, CS00); /* 64x prescaler */ }
	


/* Timer registers */
#define CLUNET_TIMER_REG TCNT0
#define CLUNET_TIMER_REG_OCR OCR0A

/* How to enable and disable timer interrupts */
#define CLUNET_ENABLE_TIMER_COMP set_bit(TIMSK0,OCIE0A)
#define CLUNET_DISABLE_TIMER_COMP unset_bit(TIMSK0, OCIE0A)
#define CLUNET_ENABLE_TIMER_OVF set_bit(TIMSK0, TOIE0)
#define CLUNET_DISABLE_TIMER_OVF unset_bit(TIMSK0, TOIE0)

/* How to init and enable external interrupt (read pin) */
#define CLUNET_INIT_INT {set_bit(EICRA,ISC00);unset_bit(EICRA,ISC01); set_bit(EIMSK, INT0);}

/* Interrupt vectors */
#define CLUNET_TIMER_COMP_VECTOR TIMER0_COMPA_vect
#define CLUNET_TIMER_OVF_VECTOR TIMER0_OVF_vect
#define CLUNET_INT_VECTOR INT0_vect

#endif
